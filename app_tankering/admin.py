from django.contrib import admin
from .models import Aircraft, Aircraft_type, Calculation


class AircraftAdmin(admin.ModelAdmin):
    list_display = ('aircraft', 'cow')


class Aircraft_typeAdmin(admin.ModelAdmin):
    list_display = ('acft_type',)


class CalculationAdmin(admin.ModelAdmin):
    list_display = ('tail', 'price_ad1', 'price_ad2', 'min_tank', 'decision')


admin.site.register(Aircraft_type, Aircraft_typeAdmin)
admin.site.register(Aircraft, AircraftAdmin)
admin.site.register(Calculation, CalculationAdmin)
