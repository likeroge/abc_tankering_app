from django.apps import AppConfig


class AppTankeringConfig(AppConfig):
    name = 'app_tankering'
