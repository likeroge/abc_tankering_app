from django.urls import path
from .views import TankeringCalculationView

urlpatterns = [
    path('', TankeringCalculationView.as_view()),
]
