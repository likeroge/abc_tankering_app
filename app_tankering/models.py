from django.db import models
from cbrf.models import DailyCurrenciesRates


# CONSTANTs for tankering calculation
AVGFF = 10.3
BIAS = 0.01
AVGPRC = 725

# модель типа


class Aircraft_type(models.Model):
    acft_type = models.CharField(max_length=20, unique=True)
    bias = models.FloatField()
    avg_ff = models.FloatField()
    avg_price = models.FloatField()

    class Meta:
        verbose_name = 'Тип ВС'
        verbose_name_plural = 'Типы ВС'

    def __str__(self):
        return self.acft_type

# модель конкретного ВС -


class Aircraft(models.Model):
    aircraft = models.CharField(max_length=10, unique=True)
    cow = models.FloatField()
    ac_type = models.ForeignKey(Aircraft_type, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Бортовой номер'
        verbose_name_plural = 'Бортовые номера'

    @staticmethod
    def get_select_tuple():
        acft_list = []
        for acft in Aircraft.objects.all():
            acft_list.append(tuple([acft.aircraft, acft.aircraft]))
        return tuple(acft_list)

    def __str__(self):
        return self.aircraft

# модель расчета танкирования


class Calculation(models.Model):
    tail = models.ForeignKey(Aircraft, on_delete=models.CASCADE)
    russian_ap = models.BooleanField(default=False)
    curs = models.FloatField()
    price_ad1 = models.FloatField()
    price_ad2 = models.FloatField()
    flight_time = models.FloatField()
    mee = models.FloatField()
    ee = models.FloatField()
    min_tank = models.FloatField()
    decision = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Расчет'
        verbose_name_plural = 'Расчеты'

    @staticmethod
    def get_curs():
        daily = DailyCurrenciesRates()
        curs = float(daily.get_by_id('R01235').value)
        return curs

    def get_correct_price_ad1(self):
        if self.russian_ap:
            correct_price_ad1 = self.price_ad1 - \
                (3000 / self.curs)
            return round(correct_price_ad1, 3)
        correct_price_ad1 = self.price_ad1
        return round(correct_price_ad1, 3)

    def get_ee(self):
        ee = (self.price_ad2 - self.price_ad1) - self.flight_time * \
            Aircraft.objects.get(
                aircraft=self.tail.aircraft).cow*self.price_ad1
        return round(ee, 3)

    def get_mee(self):
        mee = self.flight_time*AVGFF * \
            BIAS*AVGPRC
        return round(mee, 3)

    # Формула при наличии таблицы Aircraft_type с забитыми BIAS, AVG_FF, AVG_PRC
    # def get_mee(self):
    #     mee = self.flight_time * Aircraft_type.objects.get(acft_type='Boeing 747').avg_ff * Aircraft_type.objects.get(
    #         acft_type='Boeing 747').bias * Aircraft_type.objects.get(acft_type='Boeing 747').avg_price
    #     return round(mee, 3)

    def get_min_tank(self):
        try:
            min_tank = self.mee/self.ee
        except ZeroDivisionError:
            return 0
        return round(min_tank, 3)

    def get_calculation(self, form):
        self.russian_ap = eval(form.cleaned_data.get('isRusAirport'))
        self.curs = Calculation.get_curs()
        self.price_ad1 = form.cleaned_data.get('price_A1')
        self.price_ad2 = form.cleaned_data.get('price_A2')
        self.price_ad1 = self.get_correct_price_ad1()
        self.tail = Aircraft.objects.get(
            aircraft=form.cleaned_data.get('aircraft'))
        self.flight_time = form.cleaned_data.get('flight_time')
        self.ee = self.get_ee()
        self.mee = self.get_mee()
        self.min_tank = self.get_min_tank()
        if self.min_tank <= 0:
            self.decision = False
        else:
            self.decision = True
        self.save()
