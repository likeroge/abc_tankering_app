from django.shortcuts import render
from django.http import request, HttpResponse
from django.views import View
from .models import Calculation, Aircraft, Aircraft_type
from .form import TankeringForm
import os


class TankeringCalculationView(View):
    form_class = TankeringForm
    template_name = 'app_tankering/new_form.html'

    def get(self, request):
        form = self.form_class
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            calculation = Calculation()
            calculation.get_calculation(form)
            context = {'calculation': calculation, 'form': form}
            return render(request, self.template_name, context)
        return render(request, self.template_name, {'form': form})
