from django import forms
from .models import Aircraft
from django.core.validators import MinValueValidator

ACFT_CHOICES = Aircraft.get_select_tuple()

IS_RUS = (
    (True, 'РФ'),
    (False, 'NO')
)


class TankeringForm(forms.Form):
    aircraft = forms.ChoiceField(choices=ACFT_CHOICES, label='ВС', widget=forms.Select(
        attrs={'class': "form-control"}))
    isRusAirport = forms.ChoiceField(
        choices=IS_RUS, label='Аэропорт вылета - РФ?', widget=forms.Select(attrs={'class': "form-control"}))
    price_A1 = forms.FloatField(
        validators=[MinValueValidator(0.0, message='Значение меньше нуля')], widget=forms.NumberInput(attrs={'class': "form-control"}), label='Цена в АП1', help_text='Цена в аэропорту вылета')
    price_A2 = forms.FloatField(
        validators=[MinValueValidator(0.0, message='Значение меньше нуля')], widget=forms.NumberInput(attrs={'class': "form-control"}), label='Цена в АП2', help_text='Цена в аэропорту назначения')
    flight_time = forms.FloatField(
        validators=[MinValueValidator(0.0, message='Значение меньше нуля')], widget=forms.NumberInput(attrs={'class': "form-control"}), label='Время полёта', help_text='Время полёта (1 час 30 мин = 1.5)')
